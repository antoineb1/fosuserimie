'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');

/* 
gulp.task('scripts', function() {
  return gulp.src([
  	'./src/app.js',
  	'./src/app.routes.js',
  	'./src/services/*.js',
  	'./src/controllers/*.js'
  	])
    .pipe(concat('all.js'))
    .pipe(ngAnnotate())
    .pipe(uglify({
    	outSourceMap: true,
    }))
    .pipe(gulp.dest('./web/js/'));
});
*/

gulp.task('sass', function () {
  return gulp.src('./assets/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./web/css'));
});

gulp.task('watch', ['sass'/*, 'scripts'*/], function () {
  //gulp.watch('./src/**/*.js', ['scripts']);
  gulp.watch('./assets/scss/**/*.scss', ['sass']);
});